FROM jenkins/jenkins:latest
USER root
RUN apt-get update && apt-get install -y lsb-release

RUN echo 'b298a73a9fc07badfa9e4a2e86ed48824fc9201327cdc43e3f3f58b273c535e7  ./nodejs.tar.gz' > node-file-lock.sha \
  && curl -s -o nodejs.tar.gz https://nodejs.org/dist/v18.15.0/node-v18.15.0-linux-x64.tar.gz \
  && shasum --check node-file-lock.sha
RUN mkdir /usr/local/lib/nodejs \
  && tar xf nodejs.tar.gz -C /usr/local/lib/nodejs/ --strip-components 1 \
  && rm nodejs.tar.gz node-file-lock.sha
ENV PATH=/usr/local/lib/nodejs/bin:$PATH
  
RUN apt-get update && apt-get install -y curl xz-utils \
  && curl -sL "https://developer.salesforce.com/media/salesforce-cli/sf/channels/stable/sf-linux-x64.tar.xz" -o "sfdx.tar.xz" \
  && mkdir /usr/local/lib/sfdx \
  && tar -xJf sfdx.tar.xz -C /usr/local/lib/sfdx --strip-components=1 \
  && rm sfdx.tar.xz \
  && ln -s /usr/local/lib/sfdx/bin/sfdx /usr/local/bin/sfdx
ENV PATH=/usr/local/lib/sfdx/bin:$PATH


RUN apt-get update && apt-get install -y iputils-ping
RUN curl -fsSLo /usr/share/keyrings/docker-archive-keyring.asc \
  https://download.docker.com/linux/debian/gpg
RUN echo "deb [arch=$(dpkg --print-architecture) \
  signed-by=/usr/share/keyrings/docker-archive-keyring.asc] \
  https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
RUN apt-get update && apt-get install -y docker-ce-cli
USER jenkins

RUN echo y | sfdx plugins:install sfdx-git-delta
RUN sfdx plugins:install @salesforce/sfdx-scanner


RUN jenkins-plugin-cli --plugins "blueocean docker-workflow"
COPY Backup/ /var/jenkins_home/